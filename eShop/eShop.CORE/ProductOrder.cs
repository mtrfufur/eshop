﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace eShop.CORE
{
    public class ProductOrder
    {
    
        public Product Product { get; set; }

        [Key,ForeignKey("Product")]
        public int Procuct_id { get; set; }

        public Order Order { get; set; }

        [ForeignKey("Order")]
        public int Order_Id { get; set; }

    }
}
