﻿<%@ Page Title="Productos" Language="C#" MasterPageFile="~/Site.Productos.Master" AutoEventWireup="true" CodeBehind="Productos.aspx.cs" Inherits="eShop.Productos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentProductos" runat="server">

    <link href="Content/cs/style.css" rel="stylesheet" />

    <div id="productos" class="section">
        <div class="container">
            <div class="row">

                <div class="">
                    <h1 class="page_title">Productos</h1>
                    <h2 class="page_subtitle">Hardware</h2>
                    <div class="page_line"></div>
                </div>

                <div class="clear"></div>
                <div class="productos m-top-35" >

                    <div class="col-lg-3 col-md-4 col-xs-6 producto">
                        <img class="img-responsive" src="Content/Images/placabase.jpg" alt="Placa Base" />

                        <div class="team_description">
                            <h3>PLACA BASE</h3>
                            <button type="button" class="btn btn-primary">Añadir al carrito</button>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-xs-6 producto">
                        <img class="img-responsive" src="Content/Images/cpu.jpg" alt="Cpu" />

                        <div class="team_description">
                            <h3>PROCESADOR - CPU</h3>
                            <button type="button" class="btn btn-primary">Añadir al carrito</button>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-xs-6 producto">
                        <img class="img-responsive" src="Content/Images/ddr.jpg" alt="Memoria Ram" />

                        <div class="team_description">
                            <h3>MEMORIA RAM DDR4</h3>
                            <button type="button" class="btn btn-primary">Añadir al carrito</button>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-xs-6 producto">
                        <img class="img-responsive" src="Content/Images/ssd.jpg" alt="Disco duro" />

                        <div class="team_description">
                            <h3>SSD - DISCO DURO</h3>
                            <button type="button" class="btn btn-primary">Añadir al carrito</button>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-xs-6 producto">
                        <img class="img-responsive" src="Content/Images/torre.jpg" alt="Torre ordenador" />

                        <div class="team_description">
                            <h3>SEMI TORRES</h3>
                            <button type="button" class="btn btn-primary">Añadir al carrito</button>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-xs-6 producto">
                        <img class="img-responsive" src="Content/Images/fa.jpg" alt="FA Fuente de alimentación" />

                        <div class="team_description">
                            <h3>ALIMENTACIÓN</h3>
                            <button type="button" class="btn btn-primary">Añadir al carrito</button>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-xs-6 producto">
                        <img class="img-responsive" src="Content/Images/ventilacion.jpg" alt="Ventilación" />

                        <div class="team_description">
                            <h3>VENTILACIÓN</h3>
                            <button type="button" class="btn btn-primary">Añadir al carrito</button>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-xs-6 producto">
                        <img class="img-responsive" src="Content/Images/gpu.jpg" alt="Tarjeta gráfica" />

                        <div class="team_description">
                            <h3>TARJETA GRÁFICA</h3>
                            <button type="button" class="btn btn-primary">Añadir al carrito</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <link href="Content/js/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/js/bootstrap-responsive.css" rel="stylesheet" />

</asp:Content>
