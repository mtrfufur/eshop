﻿<%@ Page Title="Contacto" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contacto.aspx.cs" Inherits="eShop.Contacto" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <link href="Content/cs/estilos.css" rel="stylesheet" />

    <h3>Contacto</h3>
    <address>
        Calle Sin existencia<br />
        Camino imaginario Nº7<br />
        Teléfono: 600 000 000<br />
    </address>

    <address>
        <strong>Soporte:</strong>   <a href="mailto:soporte@eshop.com">soporte@eshop.com</a><br />
        <strong>Comercial:</strong> <a href="mailto:Comercia@eshop.com">comercial@eshop.com</a>
    </address>

    <%--Google maps--%>
      <div id="map-container-google-11" class="z-depth-1-half map-container-6" style="height: 400px">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d12337.395139696448!2d3.231246339440919!3d39.370981465658986!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses!2ses!4v1594896768496!5m2!1ses!2ses" 
            width="600" height="450"  style="border:0;" aria-hidden="false" tabindex="0"></iframe>
      </div>

</asp:Content>
