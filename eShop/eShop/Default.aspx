﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="eShop._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <link href="Content/cs/estilos.css" rel="stylesheet" />
    <div class="jumbotron">
        <%-- scripts cargados en footer Site.Master --%>
        <%-- Carrusel de fotos --%>

        <div id="Carrusel" class="carousel slide" data-ride="carousel">
            <%--Indicadores--%>
            <ol class="carousel-indicators">
                <li data-target="#Carrusel" data-slide-to="0" class="active"></li>
                <li data-target="#Carrusel" data-slide-to="1"></li>
                <li data-target="#Carrusel" data-slide-to="2"></li>
            </ol>

            <%--slider--%>
            <div class="carousel-inner">
                <div class="item active">
                    <img src="Content/images/carrusel1.jpg" alt="">
                </div>
                <div class="item">
                    <img src="Content/images/carrusel2.jpg" alt="">
                </div>
                <div class="item">
                    <img src="Content/images/carrusel3.jpg" alt="">
                </div>
            </div>

            <%--Controles--%>
            <a class="left carousel-control" href="#Carrusel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#Carrusel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>

        </div>
    </div>
    
    <div class="row">
        <div class="col-md-4">
            <h2 style="text-align:center">Rapidez</h2>
            <p>
                Servimos todos nuestros pedidos en un máximo de 24/48h.
            </p>
           
        </div>
        <div class="col-md-4">
            <h2 style="text-align:center">Confianza</h2>
            <p>
                Damos soporte gratuito sobre todos nuestros productos durante 4 años, además de sus respectivas garantías del fabricante durante 2 años.
            </p>
            
        </div>
        <div class="col-md-4">
            <h2 style="text-align:center">Comodida</h2>
            <p>
                Lo hacemos todos por ti, tan solo tienes que elegir que deseas y nosotros nos encargamos de todo.
            </p>         
        </div>
    </div>  
</asp:Content>
